-----
[YTTHpplePlus](http://git.oschina.net/dhar/YTTHpplePlus/) 是Hpple的扩展，在Hpple的基础上扩展了 HTML/XML文档 **节点的增删改查**、**文档导出**功能，原始的Hpple库可以从 [Hpple在GitHub上的链接](https://github.com/topfunky/hpple) 中查看，感谢作者原始的贡献。


- 安装
```
pod 'YTTHpplePlus'
```


#####对应功能的使用方法参考单元测试类`NewPageTest`中找到，支持的功能如下：  

- 更新或者添加属性  

```objc
- (void)testSetAttr {
    NSArray *imgs = [self.doc searchWithXPathQuery:@"//img"];
    
    for (TFHppleElement* element in imgs) {
        NSString* raw = element.raw;
        NSString* tagName = element.tagName;
        NSString* content = element.content;
        NSDictionary* attributes = element.attributes;
        
        NSString* src = [element objectForKey:@"src"];
        
    }
    
    // 设置第一个元素的属性
    if (imgs.count > 0) {
        TFHppleElement* element = imgs.firstObject;
        [self.doc setOrUpdateAttribute:@{@"width": @"13131"} inElement:element];
    }
    
    [self.doc exportXmlDoc];
    
    NSLog(@"=");
}
```

- 更新节点的内容，比如`<p>`,`<a>`,`<div>`等标签中的内容  

```objc
- (void)testSetContent {
    NSArray *paragraphes = [self.doc searchWithXPathQuery:@"//p"];
    if (paragraphes.count) {
        TFHppleElement* element = paragraphes.firstObject;
        [self.doc setOrUpdateContent:@"这个是更新替换后的内容" inElement:element];
    }
    
    [self.doc exportXmlDoc];
}
```

- 删除节点，子接点也会对应的从文档中删除  

```objc
- (void)testRemoveNode {
    NSArray *paragraphes = [self.doc searchWithXPathQuery:@"//p"];
    if (paragraphes.count) {
        TFHppleElement* element = paragraphes.firstObject;
        [self.doc deleteElement:element];
    }
    
    [self.doc exportXmlDoc];
}
```
- 导出文档，导出增删改操作之后的文档  

```objc
// 增删改查操作
NSString* exportedHtmlStr = [self.doc exportXmlDoc];
```

----
#####TODOS
- 文档导出会导致有部分双标签变成了单标签，比如`<script>`标签，这导致了web浏览器加载HTML文档出问题，目前项目中我使用了两种方法解决这个问题，希望有人如果有更好的方案，可以分享一下。
1. 正则替换`script`标签  
```objc
            NSRegularExpression *regularExpression = [NSRegularExpression regularExpressionWithPattern:@"<script.*</script>" options:0 error:nil];
            content  = [regularExpression stringByReplacingMatchesInString:content options:0 range:NSMakeRange(0, content.length) withTemplate:@""];
```
2. 在`script`标签中添加一些额外的内容，然后最终生成文档的时候再进行替换  
```html
<script src="../js/news-script.js">var abcd;</script>
```
```objc
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"<![CDATA[var abcd;]]>" withString:@""];
```
